/**
 * Created by dima on 13.10.14.
 */

    function Knight (i,team) {
        this.element = [];
        this.startX = 0;
        this.startY = 0;
        this.x = 0;
        this.y = 0;
        this.team = team;
        this.init = function () {
            if (team == 'white') {
                $('#white_box #right').append('<div class="knightWhite' + i + '"></div>');
                this.element = $('<div id="knightWhite' + i + '">&#9816</div>');
            }
            else if (team == 'black') {
                $('#black_box #left').append('<div class="knightBlack' + i + '"></div>');
                this.element = $('<div id="knightBlack' + i + '">&#9822</div>');
            }
            this.element.draggable({cursor: 'move'});
            this.move();
        };
        this.startPosition = function () {
            if (team == 'white') {
                this.startX = 1;
            }
            if (team == 'black') {
                this.startX = 8;
            }
            this.startY = 2 + (i - 1) * 5;
            this.x = this.startX;
            this.y = this.startY;
           //this.move();
        };
}

    GAME.figures.WHITEBOX.knightWhite1 = new Knight(1, 'white');
    GAME.figures.WHITEBOX.knightWhite2 = new Knight(2, 'white');
    GAME.figures.BLACKBOX.knightBlack1 = new Knight(1, 'black');
    GAME.figures.BLACKBOX.knightBlack2 = new Knight(2, 'black');

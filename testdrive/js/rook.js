/**
 * Created by dima on 14.10.14.
 */

function Rook(i,team) {
    this.element = [];
    this.startX = 0;
    this.startY = 0;
    this.x = 0;
    this.y = 0;
    this.team =  team;
    this.init = function () {
        if (team == 'white') {
            this.element = $('<div id="rookWhite'+i+'">&#9814</div>');
            $('#white_box #right').append('<div class="rookWhite'+i+'"></div>');
        }
        else if (team == 'black') {
            this.element = $('<div id="rookBlack'+i+'">&#9820</div>');
            $('#black_box #left').append('<div class="rookBlack'+i+'"></div>');
        }
        this.element.draggable({cursor : 'move'});
        this.move();
    };
    this.startPosition = function (){
        if (team == 'white'){
            this.startX = 1;
        }
        else if (team == 'black') {
            this.startX = 8;
        };
        this.startY = 1 + (i - 1)*7;
        this.x = this.startX;
        this.y = this.startY;
        $('#'+this.startX+'_'+this.startY).append(this.element);
        //this.element.css('top','0').css('left','0');
        //this.move();
    };
};

GAME.figures.WHITEBOX.rookWhite1 = new Rook(1, 'white');
GAME.figures.WHITEBOX.rookWhite2 = new Rook(2, 'white');
GAME.figures.BLACKBOX.rookBlack1 = new Rook(1, 'black');
GAME.figures.BLACKBOX.rookBlack2 = new Rook(2, 'black');

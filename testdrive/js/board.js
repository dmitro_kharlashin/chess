/**
 * Created by dima on 13.10.14.
 */

GAME.BOARD = GAME.BOARD || {
        init: function(){
            this.cells = {};
            this.initCell();
        },
        initCell: function (){
            var self = this;
            var i = 1;
            var j = 1;
            $('.cell').each(function (){
                self.initDrop(i,j);
                j++;
                if (j == 9){
                    i++;
                    j = 1;
                }
            });
        },
        initFigures: function(){
            GAME.moveFigure();
            $.each(GAME.figures, function(i,box){
                $.each(box, function(i,figure) {
                    this.init();
                    GAME.DISPATCER.moveToBox(this);
                    this.element.draggable({revert: true});
                });
            });
        },
        initDrop : function(i,j){
            var self = this;
            self.cells[i + '_' + j] = {
                x: i,
                y: j,
                cell: $('.cell#' + i + '_' + j),
                figure: {}
            };
            self.cells[i + '_' + j].cell.droppable({
                drop: function (e, ui) {
                    GAME.DISPATCER.initDeath(self.cells[i + '_' + j].figure);
                    if (!GAME.DISPATCER.initDeath(self.cells[i + '_' + j].figure)) return false;
                    GAME.draggedNow.element.draggable({revert: false});
                    self.cells[GAME.draggedNow.x + '_' + GAME.draggedNow.y].figure = {};
                    self.setFigurePosition(i,j,ui);
                    //console.log(self.cells[this.id],GAME.draggedNow);
                }
            });
        },
    setFigurePosition: function(i,j,ui){
        ui.draggable.offset({top : (this.cells[i + '_' + j].cell.offset().top+2),left :(this.cells[i + '_' + j].cell.offset().left+9)});
        var previousDraggedNowCoords = {
            x : GAME.draggedNow.x,
            y : GAME.draggedNow.y
        };
        GAME.draggedNow.x = this.cells[i+'_'+j].x;
        GAME.draggedNow.y = this.cells[i+'_'+j].y;
        this.cells[i+'_'+j].figure = GAME.draggedNow;
        $('#' + GAME.draggedNow.x + '_' + GAME.draggedNow.y).prepend(GAME.draggedNow.element);
        GAME.DISPATCER.moveComplete(previousDraggedNowCoords,GAME.draggedNow);
        //GAME.draggedNow.element.css('top', '0').css('left', '0');
    }

    };

//console.log(BOARD.cells[jQuery.inArray(this, BOARD.cells)])
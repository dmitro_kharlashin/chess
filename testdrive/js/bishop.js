/**
 * Created by dima on 14.10.14.
 */

function Bishop(i,team) {
    this.element = [];
    this.startX = 0;
    this.startY = 0;
    this.x = 0;
    this.y = 0;
    this.team =  team;
    this.init = function () {
        if (team == 'white') {
            this.element = $('<div id="bishopWhite'+i+'">&#9815</div>');
            $('#white_box #right').append('<div class="bishopWhite'+i+'"></div>');
        }
        else if (team == 'black') {
            this.element = $('<div id="bishopBlack'+i+'">&#9821</div>');
            $('#black_box #left').append('<div class="bishopBlack'+i+'"></div>');
        }
        this.element.draggable({cursor : 'move'});
        this.move();
    };
    this.startPosition = function (){
        if (team == 'white'){
            this.startX = 1;
        }
        else if (team == 'black') {
            this.startX = 8;
        }
        this.startY = 3 + (i - 1)*3;
        this.x = this.startX;
        this.y = this.startY;
        //this.element.css('top','0').css('left','0');
        //this.move();
    };
};

GAME.figures.WHITEBOX.bishopWhite1 = new Bishop(1, 'white');
GAME.figures.WHITEBOX.bishopWhite2 = new Bishop(2, 'white');
GAME.figures.BLACKBOX.bishopBlack1 = new Bishop(1, 'black');
GAME.figures.BLACKBOX.bishopBlack2 = new Bishop(2, 'black');

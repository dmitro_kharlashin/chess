/**
 * Created by dima on 09.10.14.
 */
$(document).ready(function() {
    GAME.init();
    GAME.BOARD.initFigures();
    GAME.btnStartClickInit();
    GAME.DISPATCER.init();
});


    var GAME = GAME || {
            figures : {},
            draggedNow : null,
            init : function(){
                var self = this;
                $('#init').button();
                $(".btn-slide").click(function (){
                    self.panelBoxToggle();
                });
            },
            btnStartClickInit: function(){
                var self = this;
                $('#init').on('click',function(){
                    GAME.BOARD.init();
                    self.startGame();
                    self.panelBoxToggle();
                    GAME.DISPATCER.initTeamMove('black');
                });
            },
            startGame : function(){
                $.each(GAME.figures, function(i,box){
                        $.each(box, function(i,figure){
                            this.startPosition();
                            this.element.animate({top: $('#' + this.startX + '_' + this.startY).position().top+2+'px', left: $('#' + this.startX + '_' + this.startY).position().left+9+'px'}, 1500);
                            $('#' + this.startX + '_' + this.startY).html(this.element);
                            GAME.BOARD.cells[this.startX+'_'+this.startY].figure = this;
                            this.available = true;
                            this.element.draggable({revert: false});
                        });
                });
                $.ajax({
                    url : '#',
                    type : 'POST',
                    data : {
                        start : true
                    },
                    dataType : 'json',
                    success : function(data){
                        console.log(data.ok);
                    },
                    error : function(error){
                        console.log(error);
                    }
                })
            },
            panelBoxToggle : function(){
                    $("#panel,#init").slideToggle("slow");
                $(".btn-slide").toggleClass("active");
                    return false;
            },
            moveFigure : function(){
                $.each(GAME.figures, function(i,box){
                    $.each(box, function(i,figure){
                        this.move = function () {
                            this.element.on('mousedown', function (){
                                GAME.draggedNow = figure;
                            });
                        };
                    });
                });
            }
        };
GAME.figures.WHITEBOX = GAME.figures.WHITEBOX || {

};
GAME.figures.BLACKBOX = GAME.figures.BLACKBOX || {

};


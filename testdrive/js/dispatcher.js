/**
 * Created by dima on 22.10.14.
 */
GAME.DISPATCER = GAME.DISPATCER || {
  init: function(){
      //var self = this;
      //setInterval(function(){
      //    self.initDeath();
      //}, 100);
  },
    moveToBox : function(figure){
    figure.element.css('position', 'absolute');
        //console.log(figure.element[0].id);
    if (figure.team == 'white') {
        figure.element.animate({top: $('#white_box .' + figure.element[0].id).position().top+'px', left: $('#white_box .' + figure.element[0].id).position().left+'px'}, 1500);
        $('#white_box .' + figure.element[0].id).html(figure.element);
    }
    else if (figure.team == 'black') {
        figure.element.animate({top: $('#black_box .' + figure.element[0].id).position().top+'px', left: $('#black_box .' + figure.element[0].id).position().left+'px'}, 1500);
        $('#black_box .' + figure.element[0].id).html(figure.element);
    }
},
  initDeath: function(figure){
      var self=this;
      if ((figure.element) && figure.team != GAME.draggedNow.team){
          self.moveToBox(figure);
          this.available = false;
          return true;
      }
      else if(figure.team == GAME.draggedNow.team) {
          GAME.draggedNow.element.draggable({revert: true});
          $('#' + GAME.draggedNow.x + '_' + GAME.draggedNow.y).prepend(GAME.draggedNow.element);
          //GAME.draggedNow.element.draggable({revert: false});
          return false;
      }
      return true;
  },
  moveComplete : function(previousDraggedNowCoords,draggedNow){
        if (previousDraggedNowCoords.x != draggedNow.x || previousDraggedNowCoords.y != draggedNow.y){
            $('#messages').html(draggedNow.team + ' team move is completed');
            $.ajax({
                url : '#',
                type : 'POST',
                data : {
                    figure: draggedNow.element[0].id,
                    x0: previousDraggedNowCoords.x,
                    y0: previousDraggedNowCoords.y,
                    x: draggedNow.x,
                    y: draggedNow.y
                },
                dataType : 'json',
                success : function(data){
                    if (data.ok) {
                        GAME.DISPATCER.initTeamMove(draggedNow.team);
                    }
                },
                error : function(error){
                    console.log(error);
                }
                });
        }
  },
  initTeamMove : function(team){
      if (team !='white') {
          $('#messages').html('now is white team move');
          $.each(GAME.figures.BLACKBOX, function(i,figure){
              figure.element.draggable('disable');
          });
          $.each(GAME.figures.WHITEBOX, function(i,figure){
              figure.element.draggable('enable');
          });
      }
      if (team !='black') {
          $('#messages').html('now is black team move');
          $.each(GAME.figures.WHITEBOX, function(i,figure){
              figure.element.draggable('disable');
          });
          $.each(GAME.figures.BLACKBOX, function(i,figure){
              figure.element.draggable('enable');
          });
      }
      this.timer();
  },
    timer : function(){
        var s = 0;
        var m = 0;
        var timer = setInterval(function(){
            $('#time').html(m+ ' : ' +s);
            s++;
            if (s > 59) {
               m++;
               s = 0;
            }
        },1000);
        $('.cell').mouseup(function(){
            clearInterval(timer);
        });
        $('#init').on('click',function(){
            clearInterval(timer);
        });
    }
};
/**
 * Created by dima on 14.10.14.
 */

function King(team) {
    this.element = [];
    this.startX = 0;
    this.startY = 0;
    this.x = 0;
    this.y = 0;
    this.team =  team;
    this.init = function () {
        if (team == 'white') {
            this.element = $('<div id="kingWhite">&#9812</div>');
            $('#white_box #right').append('<div class="kingWhite"></div>');
        }
        else if (team == 'black') {
            this.element = $('<div id="kingBlack">&#9818</div>');
            $('#black_box #left').append('<div class="kingBlack"></div>');
        }
            this.element.draggable({cursor : 'move'});
            this.move();
    };
    this.startPosition = function (){
        if (team == 'white'){
            this.startX = 1;
        }
        else if (team == 'black') {
            this.startX = 8;
        };
        this.startY = 5;
        this.x = this.startX;
        this.y = this.startY;
        //this.move();
    };
};

GAME.figures.WHITEBOX.kingWhite =  new King('white');
GAME.figures.BLACKBOX.kingBlack =  new King('black');

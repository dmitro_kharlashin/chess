/**
 * Created by dima on 14.10.14.
 */

function Pawn(i,team) {
    this.element = [];
    this.startX = 0;
    this.startY = 0;
    this.x = 0;
    this.y = 0;
    this.team =  team;
    this.init = function () {
        if (team == 'white') {
            this.element = $('<div id="pawnWhite'+i+'">&#9817</div>');
            $('#white_box #left').append('<div class="pawnWhite'+i+'"></div>');
        }
        else if (team == 'black') {
            this.element = $('<div id="pawnBlack'+i+'">&#9823</div>');
            $('#black_box #right').append('<div class="pawnBlack'+i+'"></div>');
        }
        this.element.draggable({cursor : 'move'});
        this.move();
    };
    this.startPosition = function (){
        if (team == 'white'){
            this.startX = 2;
        }
        else if (team == 'black') {
            this.startX = 7;
        }
        this.startY = i;
        this.x = this.startX;
        this.y = this.startY;
        //this.element.css('top','0').css('left','0');
        //this.move();
    };
};


GAME.figures.WHITEBOX.pawnWhite1 = new Pawn(1, 'white');
GAME.figures.WHITEBOX.pawnWhite2 = new Pawn(2, 'white');
GAME.figures.WHITEBOX.pawnWhite3 = new Pawn(3, 'white');
GAME.figures.WHITEBOX.pawnWhite4 = new Pawn(4, 'white');
GAME.figures.WHITEBOX.pawnWhite5 = new Pawn(5, 'white');
GAME.figures.WHITEBOX.pawnWhite6 = new Pawn(6, 'white');
GAME.figures.WHITEBOX.pawnWhite7 = new Pawn(7, 'white');
GAME.figures.WHITEBOX.pawnWhite8 = new Pawn(8, 'white');
GAME.figures.BLACKBOX.pawnBlack1 = new Pawn(1, 'black');
GAME.figures.BLACKBOX.pawnBlack2 = new Pawn(2, 'black');
GAME.figures.BLACKBOX.pawnBlack3 = new Pawn(3, 'black');
GAME.figures.BLACKBOX.pawnBlack4 = new Pawn(4, 'black');
GAME.figures.BLACKBOX.pawnBlack5 = new Pawn(5, 'black');
GAME.figures.BLACKBOX.pawnBlack6 = new Pawn(6, 'black');
GAME.figures.BLACKBOX.pawnBlack7 = new Pawn(7, 'black');
GAME.figures.BLACKBOX.pawnBlack8 = new Pawn(8, 'black');

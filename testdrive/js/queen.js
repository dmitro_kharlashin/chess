/**
 * Created by dima on 14.10.14.
 */

function Queen(team) {
    this.element = [];
    this.startX = 0;
    this.startY = 0;
    this.x = 0;
    this.y = 0;
    this.team =  team;
    this.init = function () {
        if (team == 'white') {
            this.element = $('<div id="queenWhite">&#9813</div>');
            $('#white_box #right').append('<div class="queenWhite"></div>');
        }
        else if (team == 'black') {
            this.element = $('<div id="queenBlack">&#9819</div>');
            $('#black_box #left').append('<div class="queenBlack"></div>');
        }
        this.element.draggable({cursor : 'move'});
        this.move();
    };
    this.startPosition = function (){
        if (team == 'white'){
            this.startX = 1;
        }
        else if (team == 'black') {
            this.startX = 8;
        };
        this.startY = 4;
        this.x = this.startX;
        this.y = this.startY;
        //this.element.css('top','0').css('left','0');
        //this.move();
    };
};

GAME.figures.WHITEBOX.queenWhite = new Queen('white');
GAME.figures.BLACKBOX.queenBlack = new Queen('black');

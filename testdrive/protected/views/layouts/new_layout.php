<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/chess.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.structure.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.structure.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.theme.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.theme.min.css" />
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery-1.11.1.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile  (Yii::app()->baseUrl.'/js/jquery-ui.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile  (Yii::app()->baseUrl.'/js/chess.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile  (Yii::app()->baseUrl.'/js/dispatcher.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile  (Yii::app()->baseUrl.'/js/board.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile  (Yii::app()->baseUrl.'/js/king.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile  (Yii::app()->baseUrl.'/js/queen.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile  (Yii::app()->baseUrl.'/js/knight.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile  (Yii::app()->baseUrl.'/js/rook.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile  (Yii::app()->baseUrl.'/js/bishop.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile  (Yii::app()->baseUrl.'/js/pawn.js', CClientScript::POS_END); ?>



<!--    <!-- blueprint CSS framework -->
<!--    <link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/screen.css" media="screen, projection" />-->
<!--    <link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/print.css" media="print" />-->
<!--    <!--[if lt IE 8]>-->
<!--    <link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/ie.css" media="screen, projection" />-->
<!--    <![endif]-->
<!---->
<!--    <link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/main.css" />-->
<!--    <link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/form.css" />-->

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div id="switcher"></div>

<div class="container" id="page">

    <div id="header" class="ui-widget">
        <div id="logo" class="ui-widget-header"><?php echo CHtml::encode(Yii::app()->name); ?></div>
    </div><!-- header -->


    <?php echo $content; ?>

    <div class="clear">&nbsp;</div>

    <div id="footer" class="ui-widget-content">
        Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
        All Rights Reserved.<br/>
        <?php echo Yii::powered(); ?>
    </div><!-- footer -->

</div><!-- page -->

</body>
</html>
